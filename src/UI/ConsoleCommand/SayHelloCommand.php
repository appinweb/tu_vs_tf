<?php

namespace AppInWeb\TuVsTf\UI\ConsoleCommand;

use AppInWeb\TuVsTf\Domain\Entity\HelloWorld;
use AppInWeb\TuVsTf\Domain\Repository\HelloWorldRepositoryInterface;
use AppInWeb\TuVsTf\Domain\Prioritizer\NamePrioritizerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * class ExtractFileCommand
 */
class SayHelloCommand extends Command
{
    /**
     * @var HelloWorldRepositoryInterface
     */
    private $helloWorldRepository;

    /**
     * ExtractFileCommand constructor.
     *
     * @param HelloWorldRepositoryInterface     $helloWorldRepository
     */
    public function __construct(HelloWorldRepositoryInterface $helloWorldRepository)
    {
        $this->helloWorldRepository = $helloWorldRepository;

        parent::__construct();
    }

    /**
     * Configure command
     */
    protected function configure(): void
    {
        $this
            ->setName('appinweb:say:hello')
            ->setDescription('Say Hello !')
            ->setHelp('Say given message to given name and say how many time this person was hello worlded')
        ;

        $this->addOption('msg', null, InputOption::VALUE_OPTIONAL, 'Your message', 'Hello World');
        $this->addOption('name', null, InputOption::VALUE_OPTIONAL, 'Your name', null);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $msg = $input->getOption('msg');
        $name = $input->getOption('name');

        $helloWorld = new HelloWorld(HelloWorld::generateUuid(), $msg, $name);
        $this->helloWorldRepository->save($helloWorld);

        $msgToSay = "$msg !";
        $msgCounter = 'This message was said %d time(s) until now';
        if (null !== $name) {
            $msgToSay = sprintf("$msg %s !", $name);
            $msgCounter = 'This message was said %d time(s) to %s until now';
        }
        $output->writeln($msgToSay);

        $count = $this->helloWorldRepository->getCountByMessageAndByName($msg, $name);

        $output->writeln(sprintf($msgCounter, $count, $name));
    }
}

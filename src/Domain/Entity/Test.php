<?php

namespace AppInWeb\TuVsTf\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Table(name="test")
 * @ORM\Entity()
 */
class Test
{

    /**
     * @var UuidInterface
     *
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @var HelloWorld
     *
     * @ORM\ManyToOne(targetEntity="HelloWorld", inversedBy="tests")
     * @ORM\JoinColumn(name="helloWorld_id", referencedColumnName="id")
     */
    private $helloWorld;

    /**
     * Test constructor.
     *
     * @param UuidInterface $uuid
     * @param HelloWorld    $helloWorld
     */
    public function __construct(UuidInterface $uuid, HelloWorld $helloWorld)
    {
        $this->id = $uuid;
        $this->helloWorld = $helloWorld;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return HelloWorld
     */
    public function getHelloWorld(): HelloWorld
    {
        return $this->helloWorld;
    }
}

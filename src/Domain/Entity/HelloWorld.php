<?php

namespace AppInWeb\TuVsTf\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Table(name="hello_world")
 * @ORM\Entity()
 */
class HelloWorld
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $message;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(name="created_at", type="datetime_immutable", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Test", mappedBy="helloWorld")
     */
    private $tests;

    /**
     * HelloWorld constructor.
     *
     * @param UuidInterface $uuid
     * @param string        $message
     * @param string|null   $name
     *
     * @throws \Exception
     */
    public function __construct(UuidInterface $uuid, string $message, ?string $name)
    {
        $this->id = $uuid;
        $this->message = $message;
        $this->name = $name;
        $this->createdAt = new \DateTimeImmutable();
        $this->tests = new ArrayCollection();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return Collection
     */
    public function getTests(): Collection
    {
        return $this->tests;
    }

//    public function addTest()

    /**
     * @return UuidInterface
     *
     * @throws \Exception
     */
    public static function generateUuid(): UuidInterface
    {
        return Uuid::uuid4();
    }
}

<?php
namespace AppInWeb\TuVsTf\Domain\Exception\Repository;

/**
 * class EntityNotFoundException
 */
class EntityNotFoundException extends \Exception
{
}

<?php

namespace AppInWeb\TuVsTf\Domain\Repository;

use AppInWeb\TuVsTf\Domain\Entity\HelloWorld;
use AppInWeb\TuVsTf\Domain\Exception\Repository\EntityNotFoundException;
use Ramsey\Uuid\UuidInterface;

/**
 * interface HelloWorldRepositoryInterface
 */
interface HelloWorldRepositoryInterface
{
    /**
     * @param HelloWorld $helloWorld
     */
    public function save(HelloWorld $helloWorld): void;

    /**
     * @param UuidInterface $uuid
     *
     * @return HelloWorld
     *
     * @throws EntityNotFoundException
     */
    public function getById(UuidInterface $uuid): HelloWorld;

    /**
     * @param string|null $name
     *
     * @return int
     */
    public function getCountByMessageAndByName(string $message, ?string $name): int;
}

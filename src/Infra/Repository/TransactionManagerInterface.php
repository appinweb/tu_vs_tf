<?php

namespace AppInWeb\TuVsTf\Infra\Repository;

/**
 * interface TransactionManagerInterface
 */
interface TransactionManagerInterface
{
    /**
     * @return void
     */
    public function begin(): void;

    /**
     * @return void
     */
    public function commit(): void;

    /**
     * @return void
     */
    public function rollBack(): void;
}

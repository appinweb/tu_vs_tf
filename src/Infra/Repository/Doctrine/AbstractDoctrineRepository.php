<?php

namespace AppInWeb\TuVsTf\Infra\Repository\Doctrine;

use Doctrine\ORM\EntityManagerInterface;

/**
 * class DoctrineRepository
 */
abstract class AbstractDoctrineRepository
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * Constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return EntityManagerInterface
     */
    protected function getManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }
}

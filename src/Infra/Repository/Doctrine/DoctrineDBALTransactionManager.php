<?php

namespace AppInWeb\TuVsTf\Infra\Repository\Doctrine;

use AppInWeb\TuVsTf\Infra\Repository\TransactionManagerInterface;
use Doctrine\DBAL\Connection;

/**
 * class DoctrineDBALTransactionManager
 *
 * @uses TransactionManagerInterface
 */
class DoctrineDBALTransactionManager implements TransactionManagerInterface
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public function begin(): void
    {
        $this->connection->beginTransaction();
    }

    /**
     * {@inheritdoc}
     */
    public function commit(): void
    {
        $this->connection->commit();
    }

    /**
     * {@inheritdoc}
     */
    public function rollBack(): void
    {
        $this->connection->rollBack();
    }
}

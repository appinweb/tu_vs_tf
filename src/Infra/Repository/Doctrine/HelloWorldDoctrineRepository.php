<?php

namespace AppInWeb\TuVsTf\Infra\Repository\Doctrine;

use AppInWeb\TuVsTf\Domain\Entity\HelloWorld;
use AppInWeb\TuVsTf\Domain\Exception\Repository\EntityNotFoundException;
use AppInWeb\TuVsTf\Domain\Repository\HelloWorldRepositoryInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * class HelloWorldDoctrineRepository
 */
class HelloWorldDoctrineRepository extends AbstractDoctrineRepository implements HelloWorldRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function save(HelloWorld $helloWorld): void
    {
        $this->getManager()->persist($helloWorld);
        $this->getManager()->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getById(UuidInterface $uuid): HelloWorld
    {
        $helloWorld = $this->getManager()->getRepository(HelloWorld::class)->find($uuid);

        if (!$helloWorld) {
            $id = $uuid->toString();
            throw new EntityNotFoundException("HelloWorld entity could not be found with '$id' Uuid");
        }

        return $helloWorld;
    }

    /**
     * {@inheritdoc}
     */
    public function getCountByMessageAndByName(string $message, ?string $name): int
    {
        $query = $this->entityManager->createQueryBuilder()
            ->select('count(1)')
            ->from(HelloWorld::class, 'hw')
            ->where('hw.message = :msg')
            ->setParameter('msg', $message);

        if (null !== $name) {
            $query->andWhere('hw.name = :name')
                  ->setParameter('name', $name);
        }

        return $query->getQuery()->getSingleScalarResult();
    }
}

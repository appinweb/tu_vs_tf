<?php

namespace AppInWeb\TuVsTf\App\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 */
class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @return TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $builder = new TreeBuilder();
        $rootNode = $builder->root('appinweb_tuvstf');

        $rootNode
            ->children()
                ->arrayNode('strategy')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('test')
                            ->defaultTrue()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $builder;
    }
}

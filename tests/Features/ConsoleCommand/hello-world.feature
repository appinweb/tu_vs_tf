@database
Feature:
  Test behavior of say:hello console command

  Scenario: Test behavior without any argument. The default message must be printed with no name
    Given I use fixture file 'hello-world.yml'

    When I execute 'php bin/console appinweb:say:hello'

    Then command should succeed
    And output should be:
    """
    Hello World !
    This message was said 4 time(s) until now
    """

  Scenario: Test behavior with only message argument. Correct message without name must be printed
    Given I use fixture file 'hello-world.yml'

    When I execute 'php bin/console appinweb:say:hello --msg "Ola"'

    Then command should succeed
    And output should be:
    """
    Ola !
    This message was said 1 time(s) until now
    """

  Scenario: Test behavior with only name argument. Default message targeting given name must be printed
    Given I use fixture file 'hello-world.yml'

    When I execute 'php bin/console appinweb:say:hello --name "John DOE"'

    Then command should succeed
    And output should be:
    """
    Hello World John DOE !
    This message was said 2 time(s) to John DOE until now
    """

  Scenario: Test behavior with all arguments. Correct message targeting given name must be printed
    Given I use fixture file 'hello-world.yml'

    When I execute 'php bin/console appinweb:say:hello --msg "Bien le bonjour" --name "John DOE"'

    Then command should succeed
    And output should be:
    """
    Bien le bonjour John DOE !
    This message was said 1 time(s) to John DOE until now
    """

  Scenario: Invalid command provided
    When I execute 'php bin/console appinweb:say:hello --test --retest'

    Then command should fail

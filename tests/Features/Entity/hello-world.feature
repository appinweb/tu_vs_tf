@database
Feature:
  Say given message to given name


  Scenario: Say default Hello World message
    Given I use fixture file 'hello-world.yml'

    When I find one entity having criteria 'id'='a696ee71-d54f-4217-9592-c74eeae5a742' in the repository path 'AppInWeb\TuVsTf\Domain\Entity\HelloWorld'
    Then the property path 'id' should be equal to 'a696ee71-d54f-4217-9592-c74eeae5a742'
    Then the property path 'tests' should contain 3 instances of 'AppInWeb\TuVsTf\Domain\Entity\Test'


<?php

namespace AppInWeb\TuVsTf\Tests\Features\Contexts;

use Behat\Gherkin\Node\TableNode;
use Behatch\Context\BaseContext;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * class DoctrineContext
 */
class DoctrineContext extends BaseContext
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var PropertyAccess
     */
    protected $propertyAccessor;

    /**
     * @var object
     */
    protected $entity;

    /**
     * @var string
     */
    protected $lastSqlRequest;

    /**
     * @var mixed[]
     */
    protected $dataSet;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * @param string $table
     * @param string $expected
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @Then the rows number of :table table should be equals to :expected
     */
    public function theRowsNumberOfShouldBeEqualsTo(string $table, string $expected): void
    {
        $this->executeSql(sprintf('SELECT COUNT(*) FROM %s WHERE TRUE', $table));
        $this->assertEquals(
            $expected,
            $this->dataSet[0]['count'],
            sprintf('Invalid rows number in table "%s", expected "%s" and found "%s"', $table, $expected, $this->dataSet[0]['count'])
        );
    }

    /**
     * @param string $query
     *
     * @Then execute SQL query:
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function executeSqlQuery(string $query): void
    {
        $this->executeSql($query);
    }

    /**
     * @param TableNode $expectedData
     *
     * @throws \Exception
     *
     * @Then the result set must be:
     */
    public function theResultSetMustBe(TableNode $expectedData): void
    {
        if (count($expectedData->getHash()) < count($this->dataSet)) {
            throw new \Exception(sprintf('The expected number of rows is lower than the number of rows obtained [expected: "%s", dataSet: "%s"]', count($expectedData->getHash()), count($this->dataSet)));
        }

        if (count($expectedData->getHash()) > count($this->dataSet)) {
            throw new \Exception(sprintf('The expected number of rows is greater than the number of rows obtained [expected: "%s", dataSet: "%s"]', count($expectedData->getHash()), count($this->dataSet)));
        }

        foreach ($expectedData->getHash() as $rowNum => $expected) {
            foreach ($expected as $expectedColumnName => $expectedColumnValue) {
                $columnValue = $this->dataSet[$rowNum][strtolower($expectedColumnName)];

                $this->assertEquals(
                    $expectedColumnValue,
                    $columnValue,
                    sprintf('Invalid result for query "%s", expected "%s" and found "%s"', $this->lastSqlRequest, $expectedColumnValue, $columnValue)
                );
            }
        }
    }

    /**
     * @param TableNode $expectedData
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @Then the rows number should be valid for tables:
     */
    public function theRowsNumberShouldBeValidForTables(TableNode $expectedData): void
    {
        foreach ($expectedData as $datum) {
            $this->theRowsNumberOfShouldBeEqualsTo($datum['table'], $datum['expected']);
        }
    }

    /**
     * @Then the result data set of SQL query :query should be equal to:
     *
     * @param string $query
     * @param string $expectedData
     *
     * @throws \Exception
     */
    public function theResultDataSetOfSqlQueryShouldBeEqualsTo(string $query, TableNode $expectedData): void
    {
        $this->executeSql($query);

        $this->theResultSetMustBe($expectedData);
    }

    /**
     * @param string $property
     * @param string $value
     * @param string $repositoryPath
     *
     * @throws \Exception
     *
     * @When I find one entity having criteria :property=:value in the repository path :repositoryPath
     */
    public function findEntityByCriteriaAndRepositoryPath(string $property, string $value, string $repositoryPath): void
    {
        $repository = $this->em->getRepository($repositoryPath);
        if (null === $repository) {
            throw new \Exception(sprintf('The given repository "%s" was not found.', $repositoryPath));
        }
        $this->entity = $repository->findOneBy(array($property => $value));
        if (null === $this->entity) {
            throw new EntityNotFoundException(
                sprintf('The entity having criteria %s=%s in repository "%s" does not exist.', $property, $value, $repositoryPath)
            );
        }
    }

    /**
     * @param string $propertyPath
     * @param string $value
     *
     * @throws \Exception
     *
     * @Then the property path :propertyPath should be equal to :value
     */
    public function checkPropertyEqualsTo(string $propertyPath, string $value): void
    {
        if (null === $this->entity) {
            throw new \BadMethodCallException(
                'No entity found for checking the property path. The step "When I find one entity having criteria..." should be called before.'
            );
        }
        $this->assertEquals($value, $this->propertyAccessor->getValue($this->entity, $propertyPath));
    }

    /**
     * @param string $propertyPath
     * @param string $instance
     * @param int    $nb
     *
     * @throws \Exception
     *
     * @Then the property path :propertyPath should contain :nb instances of :instance
     */
    public function checkPropertyContainsInstanceOf(string $propertyPath, int $nb, string $instance): void
    {
        if (null === $this->entity) {
            throw new \BadMethodCallException(
                'No entity found for checking the property path. The step "When I find one entity having criteria..." should be called before.'
            );
        }

        $array = $this->propertyAccessor->getValue($this->entity, $propertyPath);
        if (!is_array($array) && is_object($array)) {
            $array = $this->propertyAccessor->getValue($this->entity, $propertyPath)->toArray();
        }
        $this->assertCount($nb, $array);

        foreach ($array as $object) {
            $this->assertTrue($object instanceof $instance);
        }
    }

    /**
     * @param TableNode $table
     *
     * @throws \Exception
     *
     * @Then the properties with the paths should be equal to:
     */
    public function checkPropertiesAreEqualTo(TableNode $table): void
    {
        foreach ($table as $row) {
            $this->checkPropertyEqualsTo($row['propertyPath'], $row['value']);
        }
    }

    /**
     * @AfterScenario
     */
    public function afterScenario(): void
    {
        $this->em->getConnection()->close();
    }

    /**
     * @param string $sql
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    private function executeSql(string $sql): void
    {
        $this->lastSqlRequest = $sql; // Log for debug
        $this->dataSet = $this->em->getConnection()->query($sql)->fetchAll();
    }
}

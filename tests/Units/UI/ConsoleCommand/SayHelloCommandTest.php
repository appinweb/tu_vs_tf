<?php

namespace AppInWeb\TuVsTf\Tests\Units\UI\ConsoleCommand;

use AppInWeb\TuVsTf\Domain\Repository\HelloWorldRepositoryInterface;
use AppInWeb\TuVsTf\UI\ConsoleCommand\SayHelloCommand as SUT;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophet;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SayHelloCommandTest
 */
class SayHelloCommandTest extends TestCase
{
    const UUID = '54fce946-3e2b-4b57-ab41-fa39d312e23a';
    const MESSAGE = 'MESSAGE';
    const NAME = 'NAME';

    /**
     * @var HelloWorldRepositoryInterface
     */
    private $helloWorldRepository;

    /**
     * @var Prophet
     */
    private $prophet;

    /**
     * setUp.
     */
    protected function setUp(): void
    {
        $this->prophet = new Prophet();
    }

    /**
     * tearDown.
     */
    protected function tearDown(): void
    {
        $this->prophet->checkPredictions();
    }

    /**
     * testExecuteWithAllOptions
     */
    public function testExecuteWithAllOptions(): void
    {
        $helloWorldRepository = $this->prophesize(HelloWorldRepositoryInterface::class);
        $input = $this->prophesize(InputInterface::class);
        $output = $this->prophesize(OutputInterface::class);

        $input->getOption('msg')->willReturn(self::MESSAGE);
        $input->getOption('name')->willReturn(self::NAME);

        $helloWorldRepository->save(Argument::any())->shouldBeCalled();
        $helloWorldRepository->getCountByMessageAndByName(Argument::any(), Argument::any())->shouldBeCalled();

        $output->writeln(Argument::any())->shouldBeCalledTimes(2);
        $output->writeln(self::MESSAGE.' '.self::NAME.' !')->shouldBeCalled();

        $sut = new SUT($helloWorldRepository->reveal());

        // `execute` method is protected. We use reflection to allow the test to execute it.
        $reflection = new \ReflectionClass($sut);
        $method = $reflection->getMethod('execute');
        $method->setAccessible(true);

        $method->invoke($sut, $input->reveal(), $output->reveal());
    }
}

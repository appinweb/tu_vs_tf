<?php

namespace AppInWeb\TuVsTf\Tests\Units\Infra\Repository;

use AppInWeb\TuVsTf\Domain\Entity\HelloWorld;
use AppInWeb\TuVsTf\Infra\Repository\Doctrine\HelloWorldDoctrineRepository as SUT;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophet;
use Ramsey\Uuid\UuidInterface;

/**
 * Class HelloWorldDoctrineRepositoryTest
 */
class HelloWorldDoctrineRepositoryTest extends TestCase
{
    /**
     * @var Prophet
     */
    private $prophet;

    /**
     * setUp.
     */
    protected function setUp(): void
    {
        $this->prophet = new Prophet();
    }

    /**
     * tearDown.
     */
    protected function tearDown(): void
    {
        $this->prophet->checkPredictions();
    }


    /**
     * testSave
     */
    public function testSave(): void
    {
        $helloWorldEntity = $this->prophesize(HelloWorld::class);

        $em = $this->prophesize(EntityManagerInterface::class);
        $em->persist($helloWorldEntity->reveal())->shouldBeCalledOnce();
        $em->flush()->shouldBeCalledOnce();

        $sut = new SUT($em->reveal());
        $sut->save($helloWorldEntity->reveal());
    }

    /**
     * testGetByIdWithValidId
     */
    public function testGetByIdWithValidId(): void
    {
        $helloWorldEntity = $this->prophesize(HelloWorld::class);

        $objectRepository = $this->prophesize(ObjectRepository::class);
        $objectRepository->find(Argument::any())->willReturn($helloWorldEntity->reveal());

        $em = $this->prophesize(EntityManagerInterface::class);
        $em->getRepository(HelloWorld::class)->willReturn($objectRepository->reveal());

        $uuid = $this->prophesize(UuidInterface::class);

        $sut = new SUT($em->reveal());
        $entity = $sut->getById($uuid->reveal());

        $this->assertInstanceOf(HelloWorld::class, $entity);
    }

    /**
     * testGetByIdWithInvalidId
     *
     * @@expectedException AppInWeb\TuVsTf\Domain\Exception\Repository\EntityNotFoundException
     */
    public function testGetByIdWithInvalidId(): void
    {
        $objectRepository = $this->prophesize(ObjectRepository::class);
        $objectRepository->find(Argument::any())->willReturn(null);

        $em = $this->prophesize(EntityManagerInterface::class);
        $em->getRepository(HelloWorld::class)->willReturn($objectRepository->reveal());

        $uuid = $this->prophesize(UuidInterface::class);
        $uuid->toString()->willReturn('7f428efa-59e8-48a8-b301-99dff9df7e86');

        $sut = new SUT($em->reveal());
        $sut->getById($uuid->reveal());
    }

    /**
     * testGetCountByMessageAndByNameWithNoName
     */
    public function testGetCountByMessageAndByNameWithNoName(): void
    {
        $message = 'MESSAGE';

        // Query class is final. This is not possible to mock a final class.
        // This instruction overwrite on runtime the behavior of the class to make it classical and not final
        \uopz_flags(Query::class, null, 0);

        $query = $this->prophesize( Query::class);
        $query->getSingleScalarResult()->willReturn(42);

        $queryBuilder = $this->prophesize(QueryBuilder::class);
        $queryBuilder->select('count(1)')->willReturn($queryBuilder->reveal());
        $queryBuilder->from(HelloWorld::class, 'hw')->willReturn($queryBuilder->reveal());
        $queryBuilder->where('hw.message = :msg')->willReturn($queryBuilder->reveal());
        $queryBuilder->setParameter('msg', $message)->willReturn($queryBuilder->reveal());

        $queryBuilder->getQuery()->willReturn($query->reveal());

        $em = $this->prophesize(EntityManagerInterface::class);
        $em->createQueryBuilder()->willReturn($queryBuilder->reveal());

        $sut = new SUT($em->reveal());
        $count = $sut->getCountByMessageAndByName($message, null);

        $this->assertEquals(42, $count);
    }

    /**
     * testGetCountByMessageAndByNameWithName
     */
    public function testGetCountByMessageAndByNameWithName(): void
    {
        $message = 'MESSAGE';
        $name = 'John DOE';

        // Query class is final. This is not possible to mock a final class.
        // This instruction overwrite on runtime the behavior of the class to make it classical and not final
        \uopz_flags(Query::class, null, 0);

        $query = $this->prophesize( Query::class);
        $query->getSingleScalarResult()->willReturn(42);

        $queryBuilder = $this->prophesize(QueryBuilder::class);
        $queryBuilder->select('count(1)')->willReturn($queryBuilder->reveal());
        $queryBuilder->from(HelloWorld::class, 'hw')->willReturn($queryBuilder->reveal());
        $queryBuilder->where('hw.message = :msg')->willReturn($queryBuilder->reveal());
        $queryBuilder->andWhere('hw.name = :name')->willReturn($queryBuilder->reveal());
        $queryBuilder->setParameter('msg', $message)->willReturn($queryBuilder->reveal());
        $queryBuilder->setParameter('name', $name)->willReturn($queryBuilder->reveal());

        $queryBuilder->getQuery()->willReturn($query->reveal());

        $em = $this->prophesize(EntityManagerInterface::class);
        $em->createQueryBuilder()->willReturn($queryBuilder->reveal());

        $sut = new SUT($em->reveal());
        $count = $sut->getCountByMessageAndByName($message, $name);

        $this->assertEquals(42, $count);
    }
}

<?php

namespace AppInWeb\TuVsTf\Tests\Units\Infra\Repository;

use AppInWeb\TuVsTf\Infra\Repository\Doctrine\DoctrineDBALTransactionManager as SUT;
use Doctrine\DBAL\Connection;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophet;

/**
 * class DoctrineDBALTransactionManagerTest
 */
class DoctrineDBALTransactionManagerTest extends TestCase
{
    /**
     * @var Prophet
     */
    private $prophet;

    /**
     * setUp.
     */
    protected function setUp(): void
    {
        $this->prophet = new Prophet();
    }

    /**
     * tearDown.
     */
    protected function tearDown(): void
    {
        $this->prophet->checkPredictions();
    }

    /**
     * testBegin.
     */
    public function testBegin(): void
    {
        $conn = $this->prophesize(Connection::class);
        $conn->beginTransaction()->shouldBeCalled();

        $sut = new SUT($conn->reveal());
        $sut->begin();
    }

    /**
     * testCommit.
     */
    public function testCommit(): void
    {
        $conn = $this->prophesize(Connection::class);
        $conn->commit()->shouldBeCalled();

        $sut = new SUT($conn->reveal());
        $sut->commit();
    }

    /**
     * testRollback.
     */
    public function testRollback(): void
    {
        $conn = $this->prophesize(Connection::class);
        $conn->rollBack()->shouldBeCalled();

        $sut = new SUT($conn->reveal());
        $sut->rollBack();
    }
}

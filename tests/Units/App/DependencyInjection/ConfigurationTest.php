<?php

namespace AppInWeb\TuVsTf\Tests\Units\App\DependencyInjection;

use AppInWeb\TuVsTf\App\DependencyInjection\Configuration;
use PHPUnit\Framework\TestCase;

/**
 * class ConfigurationTest
 */
class ConfigurationTest extends TestCase
{
    /**
     * testDefaultValuesConfiguration
     */
    public function testDefaultValuesConfiguration(): void
    {
        $expectedConfig  = [
            'strategy' => [
                'test' => true,
            ],
        ];

        $inputConfig = [
            'strategy' => [
                'test' => null,
            ],
        ];

        $configuration = new Configuration();
        $node = $configuration->getConfigTreeBuilder()->buildTree();
        $normalizedConfig = $node->normalize($inputConfig);
        $finalizedConfig = $node->finalize($normalizedConfig);

        $this->assertEquals($expectedConfig, $finalizedConfig);
    }

    /**
     * testBooleanValuesConfiguration
     */
    public function testBooleanValuesConfiguration(): void
    {
        $expectedConfig  = [
            'strategy' => [
                'test' => false,
            ],
        ];

        $inputConfig = [
            'strategy' => [
                'test' => false,
            ],
        ];

        $configuration = new Configuration();
        $node = $configuration->getConfigTreeBuilder()->buildTree();
        $normalizedConfig = $node->normalize($inputConfig);
        $finalizedConfig = $node->finalize($normalizedConfig);

        $this->assertEquals($expectedConfig, $finalizedConfig);
    }

    /**
     * testInvalidValueTypesConfiguration
     *
     * @expectedException Symfony\Component\Config\Definition\Exception\InvalidTypeException
     */
    public function testInvalidValueTypesConfiguration(): void
    {
        $expectedConfig  = [
            'strategy' => [
                'test' => true,
            ],
        ];

        $inputConfig = [
            'strategy' => [
                'test' => "",
            ],
        ];

        $configuration = new Configuration();
        $node = $configuration->getConfigTreeBuilder()->buildTree();
        $normalizedConfig = $node->normalize($inputConfig);
    }

    /**
     * testEmptyConfiguration
     *
     */
    public function testEmptyConfiguration(): void
    {
        $expectedConfig = [
            'strategy' => [
                'test' => true,
            ],
        ];

        $inputConfig = [];

        $configuration = new Configuration();
        $node = $configuration->getConfigTreeBuilder()->buildTree();
        $normalizedConfig = $node->normalize($inputConfig);
        $finalizedConfig = $node->finalize($normalizedConfig);

        $this->assertEquals($expectedConfig, $finalizedConfig);
    }
}

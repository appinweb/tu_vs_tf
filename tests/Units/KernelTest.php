<?php

namespace AppInWeb\TuVsTf\Tests\Units\Domain\Entity;

use AppInWeb\TuVsTf\Kernel as SUT;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophet;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Routing\RouteCollectionBuilder;

/**
 * Class KernelTest
 */
class KernelTest extends TestCase
{
    /**
     * @var Prophet
     */
    private $prophet;

    /**
     * setUp.
     */
    protected function setUp(): void
    {
        $this->prophet = new Prophet();
    }

    /**
     * tearDown.
     */
    protected function tearDown(): void
    {
        $this->prophet->checkPredictions();
    }

    /**
     * testGetCacheDir
     */
    public function testGetCacheDir(): void
    {
        $sut =  new SUT('development', true);

        $this->assertEquals($sut->getProjectDir().'/var/cache/development', $sut->getCacheDir());
    }

    /**
     * testGetLogDir
     */
    public function testGetLogDir(): void
    {
        $sut =  new SUT('development', true);

        $this->assertEquals($sut->getProjectDir().'/var/log', $sut->getLogDir());
    }

    /**
     * testRegisterBundlesInProdEnvironment
     */
    public function testRegisterBundlesInProdEnvironment(): void
    {
        $sut =  new SUT('prod', true);

        $generator = $sut->registerBundles();

        $this->assertInstanceOf(\Symfony\Bundle\FrameworkBundle\FrameworkBundle::class, $generator->current());
        $generator->next();

        $this->assertInstanceOf(\Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle::class, $generator->current());
        $generator->next();

        $this->assertInstanceOf(\Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class, $generator->current());
        $generator->next();

        $this->assertInstanceOf( \Oneup\FlysystemBundle\OneupFlysystemBundle::class, $generator->current());
        $generator->next();

        $this->assertInstanceOf(\Symfony\Bundle\MonologBundle\MonologBundle::class, $generator->current());
        $generator->next();
    }

    /**
     * testRegisterBundlesInDevEnvironment
     */
    public function testRegisterBundlesInDevEnvironment(): void
    {
        $sut =  new SUT('dev', true);

        $generator = $sut->registerBundles();

        $this->assertInstanceOf(\Symfony\Bundle\FrameworkBundle\FrameworkBundle::class, $generator->current());
        $generator->next();

        $this->assertInstanceOf(\Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle::class, $generator->current());
        $generator->next();

        $this->assertInstanceOf(\Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class, $generator->current());
        $generator->next();

        $this->assertInstanceOf( \Oneup\FlysystemBundle\OneupFlysystemBundle::class, $generator->current());
        $generator->next();

        $this->assertInstanceOf(\Nelmio\Alice\Bridge\Symfony\NelmioAliceBundle::class, $generator->current());
        $generator->next();

        $this->assertInstanceOf(\Fidry\AliceDataFixtures\Bridge\Symfony\FidryAliceDataFixturesBundle::class, $generator->current());
        $generator->next();

        $this->assertInstanceOf(\Symfony\Bundle\MonologBundle\MonologBundle::class, $generator->current());
        $generator->next();
    }

    /**
     * testConfigureContainer
     */
    public function testConfigureContainer(): void
    {
        $sut =  new SUT('development', true);

        // `configureContainer` method is protected. We use reflection to allow the test to execute it.
        $reflection = new \ReflectionClass($sut);
        $method = $reflection->getMethod('configureContainer');
        $method->setAccessible(true);

        $containerBuilder = $this->prophesize(ContainerBuilder::class);
        $loader = $this->prophesize(LoaderInterface::class);

        $containerBuilder->addResource(Argument::any())->shouldBeCalled();
        $containerBuilder->setParameter(Argument::any(), Argument::any())->shouldBeCalled();
        $loader->load(Argument::any(), Argument::any())->shouldBeCalledTimes(4);

        $method->invoke($sut, $containerBuilder->reveal(), $loader->reveal());
    }

    /**
     * testConfigureRoutes
     */
    public function testConfigureRoutes(): void
    {
        $sut =  new SUT('development', true);

        // `configureRoutes` method is protected. We use reflection to allow the test to execute it.
        $reflection = new \ReflectionClass($sut);
        $method = $reflection->getMethod('configureRoutes');
        $method->setAccessible(true);

        $routeCollectionBuilder = $this->prophesize(RouteCollectionBuilder::class);
        $routeCollectionBuilder->import(Argument::any(), Argument::any(), Argument::any())->shouldBeCalledTimes(3);

        $method->invoke($sut, $routeCollectionBuilder->reveal());
    }

    /**
     * testBuild
     */
    public function testBuild(): void
    {
        $sut =  new SUT('development', true);

        // `configureRoutes` method is protected. We use reflection to allow the test to execute it.
        $reflection = new \ReflectionClass($sut);
        $method = $reflection->getMethod('build');
        $method->setAccessible(true);

        $containerBuilder = $this->prophesize(ContainerBuilder::class);

        $method->invoke($sut, $containerBuilder->reveal());

        $this->assertTrue(true); // Nothin to test here
    }
}

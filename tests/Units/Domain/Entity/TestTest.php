<?php

namespace AppInWeb\TuVsTf\Tests\Units\Domain\Entity;

use AppInWeb\TuVsTf\Domain\Entity\Test as SUT;
use AppInWeb\TuVsTf\Domain\Entity\HelloWorld;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophet;
use Ramsey\Uuid\UuidInterface;

/**
 * Class TestTest
 */
class TestTest extends TestCase
{
    const UUID = '54fce946-3e2b-4b57-ab41-fa39d312e23a';

    /**
     * @var Prophet
     */
    private $prophet;

    /**
     * setUp.
     */
    protected function setUp(): void
    {
        $this->prophet = new Prophet();
    }

    /**
     * tearDown.
     */
    protected function tearDown(): void
    {
        $this->prophet->checkPredictions();
    }

    /**
     * testGetId
     */
    public function testGetId(): void
    {
        $sut = $this->getSUT();

        $this->assertInstanceOf(UuidInterface::class, $sut->getId());
        $this->assertEquals(self::UUID, $sut->getId()->toString());
    }

    public function testGetHelloWorld(): void
    {
        $sut = $this->getSUT();

        $this->assertInstanceOf(HelloWorld::class, $sut->getHelloWorld());
    }

    /**
     * @return SUT
     *
     * @throws \Exception
     */
    private function getSUT(): SUT
    {
        $uuid = $this->prophesize(UuidInterface::class);
        $uuid->toString()->willReturn(self::UUID);

        $helloWorld = $this->prophesize(HelloWorld::class);

        return new SUT($uuid->reveal(), $helloWorld->reveal());
    }


}

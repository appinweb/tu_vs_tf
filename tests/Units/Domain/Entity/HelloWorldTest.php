<?php

namespace AppInWeb\TuVsTf\Tests\Units\Domain\Entity;

use AppInWeb\TuVsTf\Domain\Entity\HelloWorld as SUT;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophet;
use Ramsey\Uuid\UuidInterface;

/**
 * Class HelloWorldTest
 */
class HelloWorldTest extends TestCase
{
    const UUID = '54fce946-3e2b-4b57-ab41-fa39d312e23a';
    const MESSAGE = 'MESSAGE';
    const NAME = 'NAME';

    /**
     * @var Prophet
     */
    private $prophet;

    /**
     * setUp.
     */
    protected function setUp(): void
    {
        $this->prophet = new Prophet();
    }

    /**
     * tearDown.
     */
    protected function tearDown(): void
    {
        $this->prophet->checkPredictions();
    }

    /**
     * testGetId
     */
    public function testGetId(): void
    {
        $sut = $this->getSUT();

        $this->assertInstanceOf(UuidInterface::class, $sut->getId());
        $this->assertEquals(self::UUID, $sut->getId()->toString());
    }

    public function testGenerateUuid(): void
    {
        $this->assertInstanceOf(UuidInterface::class, SUT::generateUuid());
    }

    public function testGetTestsEmpty(): void
    {
        $sut = $this->getSUT();

        $this->assertEquals(new ArrayCollection(), $sut->getTests());
    }

    /**
     * @return SUT
     *
     * @throws \Exception
     */
    private function getSUT(): SUT
    {
        $uuid = $this->prophesize(UuidInterface::class);
        $uuid->toString()->willReturn(self::UUID);

        return new SUT($uuid->reveal(), self::MESSAGE, self::NAME);
    }


}

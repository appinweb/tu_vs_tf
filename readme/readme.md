# TEST UNITAIRE

![Tester c'est douter](./commitStrip.jpg)

## Principe
Tester une partie précise d'un code en contrôlant toutes les dépendances à ce dernier de façon à maîtriser l'environnement d'exécution.
Lors d'un test unitaire il faut prendre soin de s'affranchir de toutes les dépendances. En effet, il ne faut pas que notre test échoue à cause d'un composant qui n'est pas compris dans le bloc de code que l'on teste.

## But d'un test unitaire
Le test unitaire a pour but de tester les algorithmes. 
Un test unitaire est totalement dépendant des implémentations choisies ainsi des données d'entrées. Ainsi, si l'application utilise du XML et que demain elle utilise un autre format, il faudra ré-écrire les tests.
Le test unitaire se différentie en cela du **test fonctionnel** qui lui se moque de l'implémentation et a pour but de valider les résultats fonctionnels attendus.

## Les acteurs principaux
* **SUT**

**S**ystem **U**nder **T**est : il représente ce que l'on est en train de tester. Ce sont tous les acteurs du test qui ne sont pas des dépendances.

* **MOCK**

C'est un objet qui est une substitution complète de l’implémentation originale d’une classe concrète.
On peut piloter le code joué par celui-ci. Par exemple, je peux demander au mock de renvoyer une exception lors d'un appel à l'une de ses méthodes avec des paramètres précis.

* **STUB**

Un stub est un objet programmé pour renvoyer des réponses prédéfinies. 
Par exemple dans le cas d'une utilisation d'un composant réalisant une connection à une base de données, le _stub_ se contenterait de renvoyer des données prédéfinies au lieu d'effectuer les requêtes en base. 
Cet objet est plus simple que le mock.

Principale différence entre un mock et un stub :

* le stub va fournir des données prédéfinies, on ne veut pas se soucier de connaître le fonctionnement interne du composant
* avec le mock on est capable de piloter le code qui est joué par le mock 

## Les framework de tests unitaire 

Pour nous aider dans ces tests, plusieurs framework sont disponibles.
Ceux courament utilisés à Wake On Web : 

* [php unit](https://phpunit.de)
* [prophecy](https://github.com/phpspec/prophecy)

## Tester quoi? Comment?
### Tester quoi? 
Les tests unitaires vont servir à valider nos algorithmes.
On devra donc tester :

* les cas qui retournent le résultat attendu

Tout se passe bien, les données en entrées sont cohérentes et les données en sortie sont celles attendues.

* les cas qui entrent en erreur logique

L'application va rencontrer une erreur _logique_, c'est à dire une erreur qui peut survenir dans certains cas normaux d'exécution et qui est prévue par le programme.
C'est la _LogicExcpetion_ en php.

* les cas qui rentrent en erreur d'exécution

Quelque chose d'inattendu survient. Par exemple la connexion à la base de données n'est plus disponible. Il ne faut pas que cela entraîne l'apocalypse dans notre programme!
C'est la _RuntimeExcpetion_ en php. 

### Tester comment?

Une fois les règles de bases respectées :

* isoler le SUT
* créer des _stub_ pour les dépendances que l'on ne souhaite pas piloter
* créer des _mock_ pour les autres dépendances
* créer des fixtures afin de mettre l'environnement dans un état spécifique maîtrisé

on peut se lancer dans le test.

Nous allons écrire les tests sous forme de classe php.

* le dossier _/tests_ contient tous nos tests ainsi que les fixtures
* une classe par composant testé
* l'abrorescence du dossier _/tests_ reprend celle du projet

Ensuite, nos tests vont contrôler :

* le résultat **final** attendu
* les résultats **intermédiaires** attendus
Contrôle des paramètres reçus par les méthodes des mocks utilisés
* la levée d'exception
On va contrôler qu'un type d'exception est bien levé
* l'appel - ou le non appel - de certaines méthodes des mocks utilisés
* l'enchaînement des appels et des paramètres reçus par les méthodes des mokcs utilisés

## Vers le TDD

**T**est **D**riven **D**evelopment
Méthode de développement logiciel qui consiste à

* commencer par écrire le test...
* ... puis le code de l'application

Le but est de faire réussir le test.

Ci-dessous le cycle d'un développement TDD

![Mantra du TDD](./mantra.gif)

Cette méthode propose des avantages notables :

* on se concentre d'abord sur l'algorithme avant de se lancer dans du code et de réfléchir ensuite
* on a de ce fait une meilleure vision globale du travail à faire et l'on peut le modéliser plus efficacement
* on est plus amené à réfléchir aux différents cas d'échec et cas à la marge
* il n'y a pas de code mort : le code écrit est du code nécessaire
* on réalise les tests de notre code \o/
* le test n'est pas écrit longtemps après le dev', donc on a toute la mécanique en tête pour écrire un test pertinent

Mais il y a aussi des points sur lesquels rester vigilants :

* ne pas se laisser accaparer par le côté "test". Attention aux délais!
* être attentif à ne pas sortir du _SUT_
* ne pas perdre de vue l'objectif **métier** de ce que l'on développe


### A vos tests unitaires! =D



# TEST FONCTIONNEL

![Test fonctionnel](./engrenage.jpg)

## Principe
Tester le comportemennt d'une application d'un point de vue métier.
On va s'assurer que toutes les demandes sont traitées conformément aux spécifications.
Le test fonctionnel permet de contrôler que les données de sorties sont celles demandées par le métier. 
Dans cette catégorie de test, peu nous importe de vérifier la manière dont ces données sont calculées, seul le résultat est considéré.


## But d'un test fonctionnel
Le test fonctionnel a pour but de contrôler les données de sortie produites par une application. 
Le test fonctionnel est indépendant de l'implémentation choisie pour concevoir l'application. Si l'implémentation change, le test ne doit pas changer. Le comportement de l'application doit rester identique.

Le test fonctionnel va dérouler un ensemble de **scenarii**. On ne pourra jouer qu'avec les données **en entrées**. 

Le test fonctionnel a aussi pour objectif de servir de documentation fonctionnelle pour le  projet.

La démarche pour créer un test fonctionnel est la suivante :

* déterminer ce que l'on souhaite tester
* lister et créer un jeu de données d'entrée
* lister et créer les données attendues en sortie
* exécuter le test
* comparer les données produites avec celles attendues


## Les acteurs principaux

Exemple d'un scénario de test : 

```
@extractor @database @amqp
Feature:
  The extrator get file on the ftp and then push a RabbitMQ Message and save the file info in the DB

##########################
##### HAPPY SCENARIO #####
##########################
  @database @amqp @extractor:happy
  Background:
    Given I purge recursively all files on "/data/ftp/Dispeo"
    
  Scenario Outline: happy_scenario
    Given I push the file '<aggregateFile>' to '<destination>'

    When I execute 'php bin/console logit:extract:file'

    Then command should succeed
    And output should contain 'success'
    And I execute 'ls data/ftp/Dispeo/processing/prefix.00000.item_123456_20190123092147.xml'
    And command should succeed
    And the rows number of 'in_file' table should be equals to 1
    And I have 1 messages in amqp 'async_internal' queue
    Examples:
      | aggregateFile                        | destination                                              |
      | /tests/Fixtures/files/Item/ITEM_VP_happy.xml | /data/ftp/Dispeo/prefix.00000.item_123456_20190123092147.xml |

```

 **Gherkin**
C'est le language avec lequel on va écrire les tests.
Comme c'est le cas pour YAML ou Python, Gherkin est une language "orienté ligne", c'est-à-dire qu'il utilise les indentations pour définir sa structure. 

Gherkin est un langage "spécifique à un domaine fonctionnel, et lisible par un fonctionnel". 
Il a été imaginé spécialement pour la description de comportements. Il vous offre la possibilité d'abstraire les détails d'implémentation de vos tests de comportement.

On remarque la présence de différents mots clefs :

* **Background**
Ce mot clef, appelé **contexte** permet d'ajouter des _étapes communes_ à l'ensemble des scénarii d'une fonctionnalité en une seule fois. 
Un _contexte_ est comme un scénario : sans titre, et contient plusieurs étapes. La différence fondamentale est que le contexte sera appelé avant chacun de vos scénarios (mais juste après les _hooks_ BeforeScenario).

* **scenario**
C'est l'histoire d'un test : sa description textuelle. Il peut y avoir plusieurs scenarii pour tester une même fonctionnalité.

* **steps**
Ou _étape_ en français. Ce sont les différents étapes du scénario.
Il ne doit y avoir qu'une seule action par étape.

Chaque étépe commence par un mot clef : 

* **Given**
Décrit un état, une pré-condition pour exécuter le scénario.
* **When**
Décrit un événement, une action clef.
* **And**
Décrit une action. Ce mot clef permet de chaîner plusieurs étapes en évitant de répéter le même mot clef.

Exemple :

```
Scenario: example for 'AND' keyword
  Given it is 9 AM
  Given the sun shines
  Given there is no cloud
  When I open the door
  Then I am happy
```

est équivalent à 

```
Scenario: example for 'AND' keyword
  Given it is 9 AM
  And the sun shines
  And there is no cloud
  When I open the door
  Then I am happy
```

* **Then**
Décrit le résultat attendu.

_Note :_ Behat ne distingue pas techniquement ces trois types d'étapes. Elles sont présentes uniquement pour guider la lecture / reflexion humaine sur le scénario. 

* **Tags**
Les _tags_ permettent d'organiser les features et les scénarios.
Ils sont précédés du caractère _@_. Lors de l'exécution des tests, on pourra préciser le ou les tags que l'on souhaite jouer.

* **Contexte d'intégration**
Les classes de contextes permettent d'ajouter des _étapes_ personnalisées pour notre application.


Voir la [documentation](http://docs.behat.org/en/v2.5/guides/1.gherkin.html) pour toutes les informations.


## Les framework de tests unitaire 

Pour nous aider dans ces tests, plusieurs framework sont disponibles.
A Wake On Web on utilise : 

* [Behat](http://behat.org/en/latest/)


## Tester quoi? Comment?
### Tester quoi? 
Les tests fonctionnels vont servir à valider le comportement de l'application.
On devra donc tester :

* les cas qui retournent le résultat attendu

Tout se passe bien, les données produites sont celles attendues.

* les cas qui entrent en erreur logique

L'application va rencontrer une erreur _logique_, c'est à dire une erreur qui peut survenir dans certains cas normaux d'exécution et qui est prévue par le programme.
C'est la _LogicExcpetion_ en php.
Ici on vérifie que l'application retourne les messages d'erreurs adéquates et on pourra aussi tester les messages d'erreurs. Par exemple dans le cas d'une application de type _front office_ on va contrôler que les libellés sont les bons, et qu'ils sont aux endroits attendus.

* Mais on ne **teste pas** les cas qui rentrent en erreur d'exécution

Le test fonctionnel permet de dérouler un scénario. On ne peut modifier que les données en entrées, on ne peut pas piloter le comportement des composants qui entrent en jeu.


### Tester comment? 
#### Tester avec Behat

Behat permet d'écrire des tests écrits en **langage naturel**.
Ce qui permet en théorie à n'importe quelle personne de lire et même d'écrire un test.

Nous allons écrire les tests sous forme de fichier ayant comme extension _feature_. 
Exemple : extractor.feature

Tous les tests seront regroupés dans un dossier _/features_, puis l'on pourra créer des sous-dossiers afin d'organiser les tests.
On va créer un fichier par composant testé. Ce fichier regroupera les différents scénarios pensés pour ce composant.


## Vers le BDD

**B**ehaviour **D**riven **D**evelopment
Méthode de développement logiciel qui consiste à

* commencer par écrire le test fonctionnel...
* ... puis le code de l'application

Le but est de faire réussir le test. Le raisonnement est orienté par le côté métier (fonctionnel) de ce que l'on réalise.

Mais, pas comme ceci....

![Test fonctionnel](./commitStrip.jpg)

Egalement, il faut réaliser que malgré ses avantages, le BDD possède aussi quelques inconvénients, comme en particulier la possibilité de passer à côté de certains cas d'erreurs de logique. Comme on ne peut pas piloter les composants - les fameux _mock_ des tests unitaires - on ne pourra pas (facilement) simuler la totalité des cas possibles.





